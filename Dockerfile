FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2

COPY "cargo-add-bin" "/usr/local/bin/cargo-add"
COPY "cargo-rm-bin" "/usr/local/bin/cargo-rm"
COPY "cargo-upgrade-bin" "/usr/local/bin/cargo-upgrade"
COPY "cargo-set-version-bin" "/usr/local/bin/cargo-set-version"
